<?php namespace Bite;

use LogicException;
use Bite\Helper\HelperCollection;

class HelperManager
{

    protected $helpers = [];

    public function __construct(array $helpers = [])
    {
        foreach ($helpers as $helper) {
            $this->add($helper);
        }
    }

    public function add(HelperCollection $collection)
    {
        $this->helpers[] = $collection;
        return $this;
    }

    public function call($method, $args)
    {
        foreach ($this->helpers as $collection) {
            if ($collection->has($method)) {

                ob_start();
                $data = $collection->call($method, $args);
                if (empty($data)) {
                    $data = ob_get_clean();
                } else {
                    ob_end_clean();
                }

                return $data;
            }
        }
        throw new LogicException("No helper exists named $method");
    }

}