<?php namespace Bite;

use ArrayIterator;

class Context
{

    protected $data = [];
    protected $stack;

    public function __construct(array $data = [])
    {
        $this->add($data);
        $this->stack = [];
    }

    public function add(array $data = [])
    {
        $scope = $this->pop();
        if (!is_array($scope)) {
            $scope = [];
        }
        foreach ($data as $key => $value) {
           $this->setByDot($scope, $key, $value);
        }
        $this->push($scope, false);
        return $this;
    }

    public function get($path)
    {
        foreach (array_reverse($this->data) as $scope) {
            $subject = $this->getByDot($scope, $path);
            if (!is_null($subject)) {
                return $subject;
            }
        }
        return null;
    }

    public function has($path)
    {
        $value = $this->get($path);
        return !is_null($value) && !empty($value);
    }

    public function loop($path, $label = null)
    {
        if (!$this->has($path)) {
            return false;
        }

        if (!isset($this->stack[$path])) {
            $this->stack[$path] = new ArrayIterator((array) $this->get($path));
            $this->push([]);
        }

        $iterator = $this->stack[$path];
        $this->pop();

        if (!$iterator->valid()) {
            unset($this->stack[$path]);
            return false;
        }

        if ($label) {
            $this->push([$label => $iterator->current()]);
        } else {
            $this->push((array) $iterator->current());
        }
        $iterator->next();
        return true;
    }

    /**
     * Push new context onto the stack.
     *
     * If `$byDot` is true, key-paths will be parsed.
     *
     * @param array $context
     * @param bool $byDot Defaults to true
     * @return void
     */
    public function push(array $context, $byDot = true)
    {
        if ($byDot === true) {
            $src = $context;
            $context = [];
            foreach ($src as $key => $value) {
                $this->setByDot($context, $key, $value);
            }
        }
        // print_r($context);
        $this->data[] = $context;
    }

    public function pop()
    {
        if (count($this->data) === 0) {
            return [];
        }
        array_pop($this->data);
    }

    protected function getByDot($subject, $path)
    {
        foreach (explode('.', $path) as $segment) {
            if (isset($subject[$segment])) {
                $subject = $subject[$segment];
                if ($subject instanceof \stdClass) {
                    $subject = (array) $subject;
                }
            } else {
                return null;
            }
        }
        return $subject;
    }

    protected function setByDot(array &$subject, $key, $value)
    {
        if (is_null($key)) {
            return $subject = $value;
        }
        $keys = explode('.', $key);
        while (count($keys) > 1) {
            $key = array_shift($keys);
            if (!isset($subject[$key]) || !is_array($subject[$key])) {
                $subject[$key] = [];
            }
            $subject = &$subject[$key];
        }
        $subject[array_shift($keys)] = $value;
        return $subject;
    }

}
