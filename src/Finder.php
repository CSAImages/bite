<?php namespace Bite;

use InvalidArgumentException;

class Finder
{

    protected $dirs = [];
    protected $ext;

    public function __construct($ext = 'php', array $paths = [])
    {
        $this->ext = $ext;
        $this->in($paths, true);
    }

    public function in(array $dirs, $reset = false)
    {
        $reset and $this->dirs = [];
        foreach ($dirs as $name => $dir) {
            if (!is_dir($dir)) {
                throw new InvalidArgumentException("Not a valid template directory: $dir");
            }
            $this->dirs[$name] = $dir;
        }
    }

    public function find($path)
    {
        list($dir, $name) = $this->parseName($path);

        if (!$dir) {
            foreach ($this->dirs as $dir) {
                $found = $this->maybeLoad($dir, $name);
                if ($found) {
                    return $found;
                }
            }
            return false;
        }
        return $this->maybeLoad($dir, $name);
    }

    protected function normalize($path)
    {
        $path = str_replace('.', '/', $path);
        return trim(preg_replace('|[\\/]+|', DIRECTORY_SEPARATOR, $path), DIRECTORY_SEPARATOR.'.:');
    }

    protected function parseName($name)
    {
        $dir = false;
        $segments = explode('::', $name);
        if (count($segments) === 2) {
            $dir = $segments[0];
            $name = $this->normalize($segments[1]);
        }
        $ext = (string) pathinfo($name, PATHINFO_EXTENSION);
        if (empty($ext) && !empty($this->ext)) {
            $name .= '.'.$this->ext;
        }
        return [$dir, $name];
    }

    protected function maybeLoad($dir, $name)
    {
        if (isset($this->dirs[$dir])) {
            $dir = $this->dirs[$dir];
        }
        $path = $dir.DIRECTORY_SEPARATOR.$name;
        return file_exists($path) ? $path : false;
    }

}
