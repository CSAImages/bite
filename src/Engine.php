<?php namespace Bite;

use Exception;

class Engine
{

    protected $finder;
    protected $factory;
    protected $helpers;

    public function __construct(
        Finder $finder, 
        TemplateFactory $factory,
        HelperManager $helpers
    ) {
        $this->finder = $finder;
        $this->factory = $factory;
        $this->helpers = $helpers;
        $this->factory->setEngine($this);
    }

    public function render($path, $data = [])
    {
        return $this->factory->make($path, $data)->render();
    }

    public function getFinder()
    {
        return $this->finder;
    }

    public function getFactory()
    {
        return $this->factory;
    }

    public function getHelpers()
    {
        return $this->helpers;
    }

    public function find($path)
    {
        $realPath = file_exists($path) ? $path : $this->finder->find($path);
        if ($realPath === false) {
            throw new Exception("No template exists for $path");
        }
        return $realPath;
    }

}
