<?php namespace Bite;

use LogicException;

class Section
{
    const MODE_APPEND = 1;
    const MODE_REPLACE = 2;

    protected $name;
    protected $content;
    protected $started = false;
    protected $mode;
    protected $defaultMode = self::MODE_APPEND;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function isStarted()
    {
        return $this->started;
    }

    public function start()
    {
        $this->started = true;
        ob_start();
        return $this;
    }

    public function append()
    {
        if (!$this->isStarted()) {
            throw new LogicException("Attempted to close unopened section: {$this->name}");
        }
        $this->started = false;
        if (empty($this->mode)) {
            $this->mode = self::MODE_APPEND;
        }
        $buffer = ob_get_clean();
        $this->content = ($this->mode & self::MODE_REPLACE) 
            ? $this->content 
            : $buffer.$this->content;
        return $this;
    }

    public function replace()
    {
        if (!$this->isStarted()) {
            throw new LogicException("Attempted to close unopened section: {$this->name}");
        }
        $this->started = false;
        if (empty($this->mode)) {
            $this->mode = self::MODE_REPLACE;
        }
        $buffer = ob_get_clean();
        $this->content = ($this->mode & self::MODE_APPEND) 
            ? $buffer.$this->content 
            : $buffer;
        return $this;
    }

    public function stop()
    {
        $this->defaultMode === self::MODE_REPLACE 
            ? $this->replace() 
            : $this->append();
        return $this;
    }

    public function addToBuffer($content)
    {
        $this->content .= $content;
    }

    public function getContent()
    {
        return $this->content;
    }

}
