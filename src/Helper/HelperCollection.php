<?php namespace Bite\Helper;

interface HelperCollection
{
    public function has($method);
    public function call($method, array $args = []);
}
