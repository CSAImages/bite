<?php namespace Bite\Helper;

class DefaultHelperCollection implements HelperCollection
{

    public function lower($str) {
        return strtolower($str);
    }
    
    public function upper($str) {
        return strtoupper($str);
    }

    public function format($str, $format) {
        return sprintf($format, $str);
    }

    public function defaults($str, $default=null) {
        if (!$str) {
            return $default;
        }
        return $str;
    }

    public function replace($str, $search, $replace)
    {
        return str_replace($search, $replace, $str);
    }

    public function has($method)
    {
        return method_exists($this, $method);
    }

    public function call($method, array $args = [])
    {
        return call_user_func_array([$this, $method], $args);
    }

}
