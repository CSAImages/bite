<?php namespace Bite\Helper;

class ArrayHelperCollection implements HelperCollection
{

    protected $helpers = [];

    public function __construct(array $helpers = [])
    {
        $this->helpers = $helpers;
    }

    public function has($method) 
    {
        return isset($this->helpers[$method]);
    }

    public function call($method, array $args = [])
    {
        $cb = $this->helpers[$method];
        if (!is_callable($cb)) {
            throw new LogicException("The helper $method is not callable");
        }
        return call_user_func_array($cb, $args);
    }

}
