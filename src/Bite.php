<?php namespace Bite;

/**
 * The main API for Bite.
 */
class Bite
{

    /**
     * Return a new Bite\Engine with the default dependencies.
     *
     * @param arrat $options
     * @return Bite\Engine
     */
    public static function engine(array $options = []) {
        $options = array_merge([
            'extension' => 'php',
            'dirs' => [],
            'helpers' => []
        ], $options);

        $factory = new TemplateFactory();
        $finder = new Finder($options['extension'], $options['dirs']);
        $helpers = new HelperManager([
            new Helper\DefaultHelperCollection(),
            new Helper\ArrayHelperCollection($options['helpers'])
        ]);

        return new Engine($finder, $factory, $helpers);
    }

}
