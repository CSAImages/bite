<?php namespace Bite;

class SectionFactory
{

    protected $sections = [];
    
    public function has($name)
    {
        return isset($this->sections[$name]);
    }

    public function get($name)
    {
        if (!$this->has($name)) {
            return null;
        }
        return $this->sections[$name];
    }

    public function make($name)
    {
        if (!isset($this->sections[$name])) {
            $this->sections[$name] = new Section($name);
        }
        return $this->sections[$name];
    }

    public function makeLayout($name)
    {
        if (!isset($this->sections[$name])) {
            $this->sections[$name] = new SectionLayout($name);
        }
        return $this->sections[$name];
    }

}
