<?php namespace Bite;

use LogicException;
use IteratorAggregate;
use ArrayIterator;

/**
 * All template properties are wrapped in this class, ensuring that they will 
 * be properly escaped and that there is an easy way to access filters and 
 * property modifiers.
 */
class Property implements IteratorAggregate
{

    protected $subject;
    protected $template;

    public function __construct($subject, Template $template)
    {
        $this->subject = $subject;
        $this->template = $template;
    }

    public function escape()
    {
        return htmlspecialchars((string) $this->subject);
    }

    public function trust()
    {
        return $this->subject;
    }

    /**
     * Run a callback over each item in the Property.
     *
     * @param callable $cb
     * @return void
     */
    public function each(callable $cb)
    {
        if (!is_array($this->subject) && !is_object($this->subject)) {
            return;
        }

        foreach ($this->getIterator() as $key => $value) {
            $cb($value, $key);
        }
    }

    /** 
     * Implements functionality for IteratorAggregate. Ensures all property values are
     * wrapped in their own `Property` or, if the Property is not wrapping an array or object,
     * that an empty array is returned.
     *
     * @return \Traversable
     */
    public function getIterator() 
    {
        if (!is_array($this->subject) && !is_object($this->subject)) {
            return new ArrayIterator([]);
        }
        $subject = (array) $this->subject;
        $subject = array_map(function ($item) {
            return new Property($item, $this->template);
        }, $subject);
        return new ArrayIterator($subject);
    }

    /**
     * The default functionality for Properties is to return escaped output.
     *
     * Use `$property->trust()` to output raw data
     *
     * @return string
     */
    public function __toString()
    {
        return $this->escape();
    }

    public function __get($property)
    {
        if (is_array($this->subject) || is_object($this->subject)) {
            $subject = (array) $this->subject;
            if (isset($subject[$property])) {
                return new Property($subject[$property], $this->template);
            }
        }

        return new Property(null, $this->template);
    }

    public function __call($method, $args)
    {
        array_unshift($args, $this->subject);
        $this->subject = $this->template->getHelpers()->call($method, $args);
        return $this;
    }

}
