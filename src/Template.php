<?php namespace Bite;

use SplStack;

class Template
{

    const MODE_OUTPUT = 0;
    const MODE_BUFFER = 1;

    protected $mode = self::MODE_OUTPUT;
    protected $name;
    protected $engine;
    protected $context;
    protected $sections;
    protected $stack;
    protected $lastBuffer = '';
    protected $buffer = '';
    protected $layout = null;
    protected $layoutStack = [];
    protected $helpers;
    protected $extendDepth = 0;

    public function __construct(
        $name, 
        Engine $engine, 
        Context $context,
        HelperManager $helpers
    ) {
        $this->name = $name;
        $this->engine = $engine;
        $this->context = $context;
        $this->helpers = $helpers;
        $this->sections = new SectionFactory();
        $this->stack = new SplStack();
    }

    public function layout($layout, array $data = [])
    {
        $this->mode = self::MODE_BUFFER;

        if ($this->extendDepth > 0) {
            throw new LogicException(
                "You cannot use `layout` when inside an `extend` block. ".
                "`Template::extend` CAN be nested and will give you exactly the same result. ".
                "Best practice is to only use `layout` on top-level templates."
            );
        }

        if ($layout === $this->name) {
            throw new LogicException('A layout cannot point to itself');
        }

        $this->layout = $layout;
        $this->layoutStack[$layout] = $data;
    }

    /** 
     * Reopen a layout and extend it. Unlike the `layout` method, this
     * opens a new Section that you'll need to close. Think of it as a way
     * to include many layouts on the same page.
     *
     * @example
     *
     *      <?php $this->extend('views::layout.default') ?>
     *          <?php $this->section('content') ?>
     *              <?= $this->get('foo.bar')?>
     *          <?php $this->replace() ?>
     *      <?php $this->stop('views::layout.default') ?>
     *
     * Note that you CANNOT use `layout` while inside an `extend` block, while
     * you can nest as many `extend` blocks as you desire. Treat `layout` as a
     * convenience and use it only in top-level templates or just be careful
     * about the way you structure your views.
     *
     * @param string $layout
     * @param array $data
     * @return void
     */
    public function extend($layout, $data = [])
    {
        if ($layout === $this->name) {
            throw new LogicException('A layout cannot point to itself');
        }

        $this->mode = self::MODE_BUFFER;
        $section = $this->sections->makeLayout($layout);
        $this->extendDepth++;
        $this->context->push($data);
        $this->stack->push($section);
        $section->start();
    }

    public function insert($path, array $data = [])
    {
        $this->context->push($data);
        $partial = $this->engine->render($path, $this->context);
        $this->context->pop();
        return $partial;
    }

    public function render()
    {
        $out = $this->collect($this->name);

        while ($this->getLayout()) {
            $layout = $this->getLayout();
            $this->context->push($this->layoutStack[$this->layout]);
            $this->layout = null;
            $this->mode = self::MODE_OUTPUT;
            $out = $this->collect($layout);
            $this->context->pop();
        }

        return $out;
    }

    public function provide($path, $value = null)
    {
        if (is_array($path)) {
            $this->context->add($path);
            return $this;
        }
        $this->context->add([$path => $value]);
        return $this;
    }

    public function yeilds($path, $default = '')
    {
        if ($this->sections->has($path)) {
            return $this->sections->make($path)->getContent();
        }
        if ($this->context->has($path)) {
            return $this->get($path);
        }
        while (is_callable($default)) {
            $default = call_user_func($default, $this);
        }
        return $default;
    }

    public function section($path)
    {
        $section = $this->sections->make($path);
        $this->stack->push($section);
        $section->start();
    }

    /**
     * Get a property from the context. You can optionally provide an array of filters
     * which will be applied before the value of the property is returned.
     *
     * All values returned from this method will be escaped.
     *
     * @example
     *   
     *      $this->get('foo.bar', ['defaults' => 'nada', 'lower']);
     *
     * @param string $query Can be dot-delimited to search for a deep value.
     * @param array $filters An array of filter names. To pass arguments, optionally set them
     *                       as array values.
     * @return string
     */
    public function get($query, array $filters = null)
    {
        $value = $this->getTrusted($query, $filters);
        return $this->escape($value);
    }

    /**
     * Get a property from the context. You can optionally provide an array of filters
     * which will be applied before the value of the property is returned.
     *
     * Values will NOT be escaped using this method. Be careful.
     *
     * @param string $query
     * @param array $filters
     * @return string
     */
    public function getTrusted($query, array $filters = null)
    {
        $value = $this->context->get($query);
        if (!is_null($filters)) {
            foreach ($filters as $filter => $args) {
                if (!is_string($filter)) {
                    $filter = $args;
                    $args = [];
                }
                if (!is_array($args)) {
                    $args = [$args];
                }
                array_unshift($args, $value);
                $value = $this->helpers->call($filter, $args);
            }
        }
        return $value;
    }

    /**
     * Check if the given query exists in the context. If the value is 
     * empty, this will return false as well.
     *
     * @param string $query;
     * @return boolean
     */
    public function has($query)
    {
        return $this->context->has($query);
    }

    public function escape($value)
    {
        return htmlspecialchars((string) $value);
    }

    public function loop($query, $label = null)
    {
        return $this->context->loop($query, $label);
    }

    public function e($value)
    {
        return $this->escape($value);
    }

    public function append($path = null)
    {
        return $this->stop($path, Section::MODE_APPEND);
    }

    public function replace($path = null)
    {
        return $this->stop($path, Section::MODE_REPLACE);
    }

    public function stop($path = null, $mode = null)
    {
        $block = $this->stack->pop();
        if (!$block) {
            throw new LogicException("Attempted to close an unopened block");
        }
        if (!is_null($path)) {
            if ($block->getName() !== $path) {
                throw new LogicException("Should be closing $path, closing {$block->getName()} instead");
            }
        }

        if ($mode === Section::MODE_APPEND) {
            $block->append();
        } elseif ($mode === Section::MODE_REPLACE) {
            $block->replace();
        } else {
            $block->stop();
        }

        if ($block instanceof SectionLayout) {
            $this->mode = static::MODE_OUTPUT;
            $block->setContent($this->collect($block->getName()));
            $this->context->pop();
            $this->extendDepth--;

            // Might be a template with a layout declaration. If so, DON'T
            // switch back to output mode.
            if ($this->getLayout()) {
                $this->mode = static::MODE_BUFFER;
            }
        }

        if ($this->mode === static::MODE_OUTPUT) {
            echo $block->getContent();
        }
    }

    public function getBuffer()
    {
        return $this->lastBuffer;
    }

    public function getHelpers()
    {
        return $this->helpers;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getSection($name)
    {
        return $this->sections->get($name);
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
        return $this;
    }

    protected function collect($name)
    {
        $path = $this->engine->find($name);

        ob_start();
        require $path;
        $this->lastBuffer = $this->buffer;
        $this->buffer = trim(ob_get_clean());

        return $this->buffer;
    }

    protected function getLayout()
    {
        return $this->layout;
    }

    /**
     * Get properties wrapped in a Property class.
     *
     * @example
     *  
     *      $this->foo->bar->format('<p>%s</p>');
     *
     * @param string $prop
     * @return Property
     */ 
    public function __get($prop)
    {
        return new Property($this->context->get($prop), $this);
    }

    public function __call($method, $args)
    {
        return $this->helpers->call($method, $args);
    }

}
