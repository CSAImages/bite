<?php namespace Bite;

class TemplateFactory
{

    protected $engine;
    protected $context;

    public function __construct()
    {
        $this->context = new Context([]);
    }

    public function setEngine(Engine $engine)
    {
        $this->engine = $engine;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function share(array $data)
    {
        $this->context->push($data);
        return $this;
    }

    public function make($name, $data = [])
    {
        $context = $this->context;
        if ($data instanceof Context) {
            $context = $data;
        } else {
            $context->push($data);
        } 
        return new Template(
            $name, 
            $this->engine, 
            $context,
            $this->engine->getHelpers()
        );
    }

}
