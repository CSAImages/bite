<?php

/*
Plugin Name: Bite
Plugin URI: http://csadesign.com
Description: Bite template engine
Version: 0.0.1
Author: CSA Design
Author URI: http://csadesign.com
License: *
*/

require_once __DIR__.'/load.php';
use Bite\Bite;
use Bite\Helper\ArrayHelperCollection;

/**
 * Simple function to load the bite engine singleton.
 *
 * Designed for use in wordpress.
 *
 * @param array $options
 * @return Bite\Engine
 */
function get_template_engine($options = []) {
    static $bite = null;

    if (is_null($bite)) {
        $bite = Bite::engine($options);
    } else {
        if (isset($options['dirs'])) {
            $bite->getFinder()->in($options['dirs']);
        }
        if (isset($options['helpers'])) {
            $bite->getHelpers()->add(new ArrayHelperCollection($options['helpers']));
        }
    }

    return $bite;
}
