<?php

return [
    'lower' => function ($str) {
        return strtolower($str);
    },
    'upper' => function ($str) {
        return strtoupper($str);
    },
    'format' => function ($str, $format) {
        return sprintf($format, $str);
    },
    'defaults' => function ($str, $default=null) {
        if (!$str) {
            return $default;
        }
        return $str;
    }
];
