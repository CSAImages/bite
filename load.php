<?php 

/**
 * Setup Bite with no autoloading.
 */

require_once __DIR__.'/src/Bite.php';
require_once __DIR__.'/src/Context.php';
require_once __DIR__.'/src/Engine.php';
require_once __DIR__.'/src/Finder.php';
require_once __DIR__.'/src/HelperManager.php';
require_once __DIR__.'/src/Property.php';
require_once __DIR__.'/src/Section.php';
require_once __DIR__.'/src/SectionLayout.php';
require_once __DIR__.'/src/SectionFactory.php';
require_once __DIR__.'/src/Template.php';
require_once __DIR__.'/src/TemplateFactory.php';
require_once __DIR__.'/src/Helper/HelperCollection.php';
require_once __DIR__.'/src/Helper/ArrayHelperCollection.php';
require_once __DIR__.'/src/Helper/DefaultHelperCollection.php';
