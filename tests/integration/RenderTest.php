<?php

use PHPUnit_Framework_TestCase as TestCase;
use Bite\Bite;

class RenderTest extends TestCase
{

    protected function newEngine()
    {
        return Bite::engine([
            'dirs' => [
                'one' => __DIR__.'/../fixtures/one'
            ]
        ]);
    }

    public function testRenderSimple()
    {
        $engine = $this->newEngine();
        $tpl = $engine->render('one::simple', ['foo' => 'bar']);
        $this->assertEquals('foo: bar', $tpl);
    }

    public function testRenderExtends()
    {
        $engine = $this->newEngine();
        $tpl = $engine->render('one::extends', ['foo' => 'bar']);
        $this->assertEquals('open bar close', $tpl);
    }

}