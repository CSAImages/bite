<?php 

use PHPUnit_Framework_TestCase as TestCase;
use Bite\Finder;

const DIR_FIXTURES = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'fixtures';

class FinderTest extends TestCase
{

    public function newFinder()
    {
        return new Finder('php', [
            'one' => DIR_FIXTURES.DIRECTORY_SEPARATOR.'one',
            'two' => DIR_FIXTURES.DIRECTORY_SEPARATOR.'two'
        ]);
    }

    public function testFind()
    {
        $finder = $this->newFinder();
        $this->assertEquals(DIR_FIXTURES.DIRECTORY_SEPARATOR.'one'.DIRECTORY_SEPARATOR.'foo.php', $finder->find('one::foo'));
        $this->assertEquals(false, $finder->find('two::foo'));
    }

    public function testFindDeep()
    {
        $finder = $this->newFinder();
        $this->assertEquals(DIR_FIXTURES.DIRECTORY_SEPARATOR.'one'.DIRECTORY_SEPARATOR.'layout'.DIRECTORY_SEPARATOR.'bar.php', $finder->find('one::layout.bar'));
    }

}
