<?php

use PHPUnit_Framework_TestCase as TestCase;
use Bite\Helper\DefaultHelperCollection;

class DefaultHelperCollectionTest extends TestCase
{

    public function testCall()
    {
        $helpers = new DefaultHelperCollection();
        $this->assertEquals('foo', $helpers->call('defaults', ['', 'foo']));
        $this->assertEquals('foo/bin', $helpers->call('replace', ['foo.bin', '.', '/']));
        $this->assertEquals('foo bin', $helpers->call('format', ['bin', 'foo %s']));
    }

    public function testHas()
    {
        $helpers = new DefaultHelperCollection();
        $this->assertEquals(method_exists($helpers, 'replace'), $helpers->has('replace'));
        $this->assertEquals(method_exists($helpers, 'foobar'), $helpers->has('foobar'));
    }

}
