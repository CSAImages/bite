<?php

use PHPUnit_Framework_TestCase as TestCase;
use Bite\Helper\ArrayHelperCollection;

class ArrayHelperCollectionTest extends TestCase
{

    public function testCall()
    {
        $helpers = new ArrayHelperCollection([
            'baritize' => function ($str) {
                return $str.'bar';
            }
        ]);
        $this->assertEquals('foobar', $helpers->call('baritize', ['foo']));
    }

    public function testHas()
    {
        $helpers = new ArrayHelperCollection([
            'baritize' => function ($str) {
                return $str.'bar';
            }
        ]);
        $this->assertTrue($helpers->has('baritize'));
    }

}
