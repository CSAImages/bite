<?php

use PHPUnit_Framework_TestCase as TestCase;
use Bite\Section;

class SectionTest extends TestCase
{

    public function testConstruct()
    {
        $section = new Section('foo');
        $this->assertEquals('foo', $section->getName());
    }

    public function testEncapsulates()
    {
        $section = new Section('foo');
        $section->start();
        echo 'bar';
        $section->stop();
        $this->assertEquals('bar', $section->getContent());
    }

    public function testAppend()
    {
        $section = new Section('foo');
        $section->start();
        echo 'bar';
        $section->append();
        $section->start();
        echo 'foo';
        $section->stop();
        $this->assertEquals('foobar', $section->getContent());
    }

    public function testReplace()
    {
        $section = new Section('foo');
        $section->start();
        echo 'foo';
        $section->replace();
        $section->start();
        echo 'bar';
        $section->append();
        $this->assertEquals('foo', $section->getContent());
    }

    public function testStopUsesFirstMode()
    {
        $section = new Section('foo');
        $section->start();
        echo 'foo';
        $section->append();
        $section->start();
        echo 'bar';
        $section->replace();
        $section->start();
        echo 'bin';
        $section->stop();
        $this->assertEquals('binbarfoo', $section->getContent());
    }

}
