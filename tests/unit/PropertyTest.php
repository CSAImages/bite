<?php

use PHPUnit_Framework_TestCase as TestCase;
use Mockery as m;
use Bite\Property;

class PropertyTest extends TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function mockTemplate()
    {
        $template = m::mock('Bite\Template');
        return $template;
    }

    public function testEscape()
    {
        $template = $this->mockTemplate();
        $prop = new Property('foo', $template);
        $this->assertEquals('foo', $prop->escape());
        $this->assertEquals('foo', $prop.'');
    }

    public function testHelperProxy()
    {
        $template = $this->mockTemplate();
        $helpers = m::mock('Bite\Helpers');
        $template->shouldReceive('getHelpers')
            ->andReturn($helpers)
            ->once();
        $helpers->shouldReceive('call')
            ->with('baratize', ['foo'])
            ->andReturn('bar')
            ->once();

        $prop = new Property('foo', $template);
        $this->assertEquals('bar', $prop->baratize()->escape());
    }

    public function testNestedProperties()
    {
        $prop = new Property(['foo' => ['bar' => 'bin']], $this->mockTemplate());
        $this->assertInstanceOf(Property::class, $prop->foo);
        $this->assertInstanceOf(Property::class, $prop->foo->bar);
        $this->assertEquals('bin', $prop->foo->bar->escape());
    }

    public function testIteration()
    {
        $prop = new Property(['one', 'two', 'three'], $this->mockTemplate());
        $expected = '';
        foreach ($prop as $value) {
            $expected .= $value->escape();
        }
        $this->assertEquals('onetwothree', $expected);
    }

}
