<?php

use PHPUnit_Framework_TestCase as TestCase;
use Mockery as m;
use Bite\HelperManager;

class HelperManagerTest extends TestCase
{

    public function testForwardHelperCalls()
    {
        $helper = m::mock('Bite\Helper\HelperCollection');
        $helper->shouldReceive('has')->with('foo')->andReturn(true)->once();
        $helper->shouldReceive('call')->with('foo', ['bar'])->andReturn('foobar')->once();
        $helperTwo = m::mock('Bite\Helper\HelperCollection');
        $helperTwo->shouldReceive('has')->with('foo')->andReturn(false)->once();

        $manager = new HelperManager([
            $helperTwo,
            $helper
        ]);
        $this->assertEquals('foobar', $manager->call('foo', ['bar']));
    }

}
