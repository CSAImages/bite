<?php

use PHPUnit_Framework_TestCase as TestCase;
use Bite\Context;

class ContextTest extends TestCase
{

    protected function newContext()
    {
        return new Context([
            'foo' => 'foo',
            'bar' => 'bar',
            'stuff' => [
                'foo' => 'foo',
                'more' => [
                    'foo' => 'foo',
                    'bar' => 'bar'
                ]
            ]
        ]);
    }

    public function testGet() 
    {
        $ctx = $this->newContext();
        $this->assertEquals('foo', $ctx->get('foo'));
        $this->assertEquals('bar', $ctx->get('bar'));
    }

    public function testGetByDot()
    {
        $ctx = $this->newContext();
        $this->assertEquals('foo', $ctx->get('stuff.foo'));
        $this->assertEquals('bar', $ctx->get('stuff.more.bar'));
        $this->assertEquals([
            'foo' => 'foo',
            'bar' => 'bar'
        ], $ctx->get('stuff.more'));
    }

    public function testAdd()
    {
        $ctx = $this->newContext();
        $ctx->add(['hey' => 'man']);
        $this->assertEquals('man', $ctx->get('hey'));
        $ctx->add([
            'thingie' => [
                'bif' => 'baf'
            ]
        ]);
        $this->assertEquals('baf', $ctx->get('thingie.bif'));
    }

    public function testAddByDot()
    {
        $ctx = $this->newContext();
        $ctx->add([
            'added.bar' => 'bin',
            'addedToo.bar.bin.bax' => 'bif'
        ]);
        $this->assertEquals(['bar' => 'bin'], $ctx->get('added'));
        $this->assertEquals(['bar' => ['bin' => ['bax' => 'bif']]], $ctx->get('addedToo'));
    }

    public function testPush()
    {
        $ctx = new Context(['foo' => 'foo']);
        $ctx->push(['foo' => 'changed but not replaced']);
        $this->assertEquals('changed but not replaced', $ctx->get('foo'));
        $ctx->pop();
        $this->assertEquals('foo', $ctx->get('foo'));
    }

    public function testPushDeep()
    {
        $ctx = new Context(['foo' => 'foo']);
        $ctx->push(['foo' => 'changed but not replaced']);
        $this->assertEquals('changed but not replaced', $ctx->get('foo'));
        $ctx->push(['foo' => 'changed again']);
        $this->assertEquals('changed again', $ctx->get('foo'));
        $ctx->pop();
        $this->assertEquals('changed but not replaced', $ctx->get('foo'));
        $ctx->pop();
        $this->assertEquals('foo', $ctx->get('foo'));
    }

    public function testPushByDot()
    {
        $ctx = new Context(['foo' => ['bar' => 'bar']]);
        $ctx->push(['foo.bar' => 'changed but not replaced']);
        $this->assertEquals('changed but not replaced', $ctx->get('foo.bar'));
        $ctx->pop();
        $this->assertEquals('bar', $ctx->get('foo.bar'));
    }

    public function testLoop()
    {
        $ctx = new Context(['posts' => [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3]
        ]]);
        $val = '';
        while ($ctx->loop('posts')) {
            $val .= $ctx->get('id');
        }
        $this->assertEquals('123', $val);
    }

    public function testLoopLabeled()
    {
        $ctx = new Context(['posts' => [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3]
        ]]);
        $val = '';
        while ($ctx->loop('posts', 'post')) {
            $val .= $ctx->get('post.id');
        }
        $this->assertEquals('123', $val);
    }

    public function testLoopLabeledValues()
    {
        $ctx = new Context(['vals' => [
            1,
            2,
            3
        ]]);
        $val = '';
        while ($ctx->loop('vals', 'val')) {
            $val .= $ctx->get('val');
        }
        $this->assertEquals('123', $val);
    }

}
