<?php 

use PHPUnit_Framework_TestCase as TestCase;
use Mockery as m;
use org\bovigo\vfs\vfsStream;
use Bite\Template;


class TemplateTest extends TestCase
{

    public function setUp()
    {
        vfsStream::setup('test', null, [
            'template.php' => '<foo>bar</foo>'
        ]);
    }

    public function tearDown()
    {
        m::close();
    }

    public function mockEngine()
    {
        return m::mock('Bite\Engine');
    }

    public function mockContext()
    {
        return m::mock('Bite\Context');
    }

    public function mockHelperManager()
    {
        return m::mock('Bite\HelperManager');
    }

    public function testTemplateEscape()
    {
        $ctx = $this->mockContext();
        $ctx->shouldReceive('get')->with('foo')->andReturn('<div>foo</div>')->twice();

        $template = new Template(
            'foo',
            $this->mockEngine(),
            $ctx,
            $this->mockHelperManager()
        );

        $this->assertEquals('&lt;div&gt;foo&lt;/div&gt;', $template->get('foo'));
        $this->assertEquals('<div>foo</div>', $template->getTrusted('foo'));
    }

    public function testPropertiesAreReturnedViaMagic()
    {
        $ctx = $this->mockContext();
        $ctx->shouldReceive('get')->with('foo')->andReturn('<div>foo</div>')->twice();

        $template = new Template(
            'foo',
            $this->mockEngine(),
            $ctx,
            $this->mockHelperManager()
        );

        $this->assertEquals('&lt;div&gt;foo&lt;/div&gt;', $template->foo.'');
        $this->assertEquals('<div>foo</div>', $template->foo->trust());
    }

    public function testSections()
    {
        $ctx = $this->mockContext();
        $engine = $this->mockEngine();

        $template = new Template('foo', $engine, $ctx, $this->mockHelperManager());
        $template->setMode(Template::MODE_BUFFER);
        $template->section('content');
            echo 'this should be cached';
        $template->stop();

        $this->assertEquals('this should be cached', $template->yeilds('content'));
    }

    public function testSectionLayouts()
    {
        $ctx = $this->mockContext();
        $ctx->shouldReceive('push')->with(['foo' => 'foo'])->once();
        $ctx->shouldReceive('pop')->once();

        $engine = $this->mockEngine();
        $engine->shouldReceive('find')->with('test::template')->andReturn(vfsStream::url('test/template.php'))->once();

        $template = new Template(
            'foo',
            $engine,
            $ctx,
            $this->mockHelperManager()
        );

        ob_start();
        $template->extend('test::template', ['foo' => 'foo']);
            $template->section('content');
                echo 'this should not appear';
            $template->stop();
        $template->stop();
        ob_end_clean();

        $this->assertEquals('<foo>bar</foo>', $template->getSection('test::template')->getContent());
    }

}
